from kivy.app import App
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label


class TestApp(App):

 def alert(self,inst):
  self.lb1.text="AAAAAA"
  pass
 
 def build(self):
  box1=BoxLayout(orientation="vertical",padding=[100])
  self.lb1=Label(text="HELLO WORLD")
  btn1 = Button(text="Press me", on_press=self.alert)
  box1.add_widget(self.lb1)
  box1.add_widget(btn1)
  return box1
 
if __name__ == "__main__":
 TestApp().run()
